package duedate

import Day._
import DateTime._

object DueDate {

  def countCalendarDaysFromHours(date: Date, hoursToWork: Int): (Int, Int) = {
    val dayIdOfDate = date.calculateDayOfWeek.id

    val weeksToWork = hoursToWork / WorkHoursPerWeek
    val hoursRestFromWeek = hoursToWork % WorkHoursPerWeek

    val daysToWork = hoursRestFromWeek / WorkHoursPerDay
    val hoursRestFromDay = hoursRestFromWeek % WorkHoursPerDay

    val numIdsToWork = (dayIdOfDate to (dayIdOfDate + daysToWork)).toList
    val saturdayEndComp = if (numIdsToWork.last % 7 == 5) 1 else 0
    val numOfWeekEndDays = numIdsToWork.filter(id => Day(id % 7).isWeekend).size + saturdayEndComp

    (weeksToWork * 7 + daysToWork + numOfWeekEndDays, hoursRestFromDay)
  }

  def addTimeToDateAndTime(date: Date, hoursRest: Int, timeRest: Time): (Date, Time) = {
    if ( hoursRest + timeRest.hour < WorkHoursPerDay) {
      (date, Time(WorkHoursRange._1 + hoursRest + timeRest.hour, timeRest.minute, timeRest.sec))
    } else {
      val hoursNextDay = hoursRest + timeRest.hour - WorkHoursPerDay

      val daysToPass = if (date.calculateDayOfWeek.id == 4) {
        3
      } else {
        1
      }
      val nextDay = countDueDateFromDays(date, daysToPass)
      (nextDay, Time(WorkHoursRange._1 + hoursNextDay, timeRest.minute, timeRest.sec))
    }
  }

  def dateFromYearStart(year: Int, numOfDays: Int): Date = {
    val daysInMonths = if (Date.isLeapYear(year)) {
      DaysInMonthsLeapYear
    } else {
      DaysInMonths
    }

    val (monthResult, daysAcc) = daysInMonths.toSeq
      .sortBy(_._1)
      .scan(1, 0)((m1, m2) => (m2._1, m1._2 + m2._2))
      .filterNot(_._2 < numOfDays)
      .head

    val dayResult = daysInMonths(monthResult) - (daysAcc - numOfDays)
    Date(year, monthResult, dayResult)
  }

  def countDueDateFromDays(date: Date, daysToPass: Int): Date = {
    val numOfDaysRestInYear = date.numOfDaysRestInYear
    if (daysToPass <= numOfDaysRestInYear) {
      val daysInMonths = if (Date.isLeapYear(date.year)) {
        DaysInMonthsLeapYear
      } else {
        DaysInMonths
      }

      val numOfDaysRestInMonth = daysInMonths(date.month) - date.day

      if (daysToPass <= numOfDaysRestInMonth) {
        Date(date.year, date.month, date.day + daysToPass)
      } else {
        val daysFromNextMonth = daysToPass - numOfDaysRestInMonth

        val (monthResult, daysAcc) = daysInMonths.toSeq
          .sortBy(_._1)
          .drop(date.month)
          .scan(date.month + 1, 0)((m1, m2) => (m2._1, m1._2 + m2._2))
          .filterNot(_._2 < daysFromNextMonth)
          .head

        val dayResult = daysInMonths(monthResult) - (daysAcc - daysFromNextMonth)
        Date(date.year, monthResult, dayResult)
      }

    } else {
      val daysToPassRest = daysToPass - numOfDaysRestInYear

      val nextYearLength = if (Date.isLeapYear(date.year + 1)) NumOfDaysInYear + 1 else NumOfDaysInYear
      if (daysToPassRest <= nextYearLength) {
        dateFromYearStart(date.year + 1, daysToPassRest)
      } else {
        val numOfYearsCeil = (daysToPassRest / DaysPerYearGregorian).ceil.toInt
        val (yearResult, daysAcc) = ((date.year + 1) to (date.year + 1 + numOfYearsCeil))
          .map(y => if (Date.isLeapYear(y)) (y, NumOfDaysInYear + 1) else (y, NumOfDaysInYear))
          .scan((date.year + 1, 0))((y1, y2) => (y2._1, y1._2 + y2._2))
          .filter(_._2 < daysToPassRest)
          .last

        val daysRest = daysToPassRest - daysAcc
        dateFromYearStart(yearResult + 1, daysRest)
      }
    }
  }

  def calculateDueDate(submitDate: Date, submitTime: Time, turnAroundTime: Int): (Date, Time) = {
    require(submitTime.hour >= WorkHoursRange._1, s"Submit time should be after ${WorkHoursRange._1} hour")
    require(submitTime.hour < WorkHoursRange._2, s"Submit time should be strictly before ${WorkHoursRange._2} hour")

    val dayOfSubmit = submitDate.calculateDayOfWeek

    require(dayOfSubmit.id < 5, "Submit day should not be on Weekend")

    val timeAfterWorkDayStart = Time(submitTime.hour - WorkHoursRange._1, submitTime.minute, submitTime.sec)
    val (daysToPass, hoursRest) = countCalendarDaysFromHours(submitDate, turnAroundTime)
    val dueDateWithoutHoursRest = countDueDateFromDays(submitDate, daysToPass)

    addTimeToDateAndTime(dueDateWithoutHoursRest: Date, hoursRest, timeAfterWorkDayStart): (Date, Time)
  }
}
