package duedate

object App {
  def main(args: Array[String]): Unit = {
    val date = Date(2020, 1, 28)
    val time = Time(16, 12, 0)
    val turnAroundTime = 193
    val (dueDate, dueTime) = DueDate.calculateDueDate(date, time, turnAroundTime)
    println(s"dueDate: ${dueDate}, dueTime: ${dueTime}")
  }
}
