package duedate

import duedate.DateTime.repr

case class Time(hour: Int, minute: Int, sec: Int) {

  enforceConstraints

  private def enforceConstraints = {
    require(hour >= 0, "Hour should be bigger, than 0")
    require(hour <= 23, "Hour should not be more than 23")
    require(minute >= 0, "Minute should be bigger, than 0")
    require(minute <= 59, "Minute should not be more than 59")
    require(sec >= 0, "Sec should be bigger, than 0")
    require(sec <= 59, "Sec should not be more than 59")
  }

  override def toString: String = {
    s"${repr(hour)}-${repr(minute)}-${repr(sec)}"
  }
}
