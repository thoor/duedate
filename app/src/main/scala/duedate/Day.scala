package duedate

object Day extends Enumeration {
  protected case class Val(isWeekend: Boolean) extends super.Val

  implicit def valueToDayVal(day: Value): Val = day.asInstanceOf[Val]

  type Day = Value

  val Monday = Val(false)
  val Tuesday = Val(false)
  val Wednesday = Val(false)
  val Thursday = Val(false)
  val Friday = Val(false)
  val Saturday = Val(true)
  val Sunday = Val(true)
}
