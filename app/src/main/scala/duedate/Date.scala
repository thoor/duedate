package duedate

import duedate.Date.isLeapYear
import duedate.DateTime.{DayOfWeekFixedPoint, DaysInMonths, DaysInMonthsLeapYear, NumOfDaysInYear, repr}
import duedate.Day.Day

case class Date(year: Int, month: Int, day: Int) {

  enforceConstraints

  private def enforceConstraints = {
    require(year >= 0, "Year should be bigger, than 0")
    require(month > 0, "Month should be bigger, than 0")
    require(month <= 12, "Month should not be more than 12")
    require(day > 0, "Day should be bigger, than 0")
    val numOfDaysInMonth = if (isLeapYear(year)) {
      DaysInMonthsLeapYear(month)
    } else {
      DaysInMonths(month)
    }
    require(day <= numOfDaysInMonth, s"Day should not be more than ${numOfDaysInMonth}")
  }

  def numOfDaysPassedInYear: Int = {
    if (isLeapYear(year)) {
      (1 until month).map(DaysInMonthsLeapYear(_)).sum + day
    } else {
      (1 until month).map(DaysInMonths(_)).sum + day
    }
  }

  def numOfDaysRestInYear: Int = {
    if (isLeapYear(year)) {
      NumOfDaysInYear + 1 - numOfDaysPassedInYear
    } else {
      NumOfDaysInYear - numOfDaysPassedInYear
    }
  }

  def isBiggerThan(other: Date): Boolean = {
    (this.year > other.year) ||
      ((this.year == other.year) && (this.month > other.month)) ||
        ((this.year == other.year) && (this.month == other.month) && (this.day >= other.day))
  }

  def numOfDaysBetween(other: Date): Int = {
    val (smaller, bigger) = if (isBiggerThan(other)) (other, this) else (this, other)
    if (bigger.year == smaller.year) {
      bigger.numOfDaysPassedInYear - smaller.numOfDaysPassedInYear
    } else {
      val numOfDaysInIntermittentYears = (smaller.year + 1 until bigger.year).map(year => if(isLeapYear(year)) NumOfDaysInYear + 1 else NumOfDaysInYear).sum
      smaller.numOfDaysRestInYear + numOfDaysInIntermittentYears + bigger.numOfDaysPassedInYear
    }
  }

  def calculateDayOfWeek: Day = {
    val daysFromFixPoint = numOfDaysBetween(DayOfWeekFixedPoint._1)
    if (isBiggerThan(DayOfWeekFixedPoint._1)) {
      Day(daysFromFixPoint % 7)
    } else {
      if (daysFromFixPoint % 7 == 0) {
        Day(daysFromFixPoint % 7)
      } else {
        Day(7 - (daysFromFixPoint % 7))
      }
    }
  }

  override def toString: String = {
    s"${year}-${repr(month)}-${repr(day)}"
  }
}

object Date {
  def isLeapYear(year: Int): Boolean = {
    (year % 4 == 0) &&
      ((year % 100 != 0) || (year % 400 == 0))
  }
}
