package duedate

import duedate.Day.{Day, Monday}

object DateTime {
  val DaysInMonths: Map[Int, Int] = Map(
    1 -> 31,
    2 -> 28,
    3 -> 31,
    4 -> 30,
    5 -> 31,
    6 -> 30,
    7 -> 31,
    8 -> 31,
    9 -> 30,
    10 -> 31,
    11 -> 30,
    12 -> 31
  )

  val DaysInMonthsLeapYear: Map[Int, Int] = DaysInMonths.updated(2, 29)

  val WorkHoursRange: (Int, Int) = (9, 17)

  val NumOfDaysInYear = 365

  val DayOfWeekFixedPoint: (Date, Day) = (Date(2020, 11, 30), Monday)

  val WorkHoursPerDay = 8

  val WorkHoursPerWeek = 5 * WorkHoursPerDay

  val DaysPerYearGregorian = 365.2425

  def repr(dateTimePart: Int): String = if (dateTimePart < 10) "0" + dateTimePart else dateTimePart.toString
}
