package duedate

import org.scalatest.funsuite.AnyFunSuite
import org.junit.runner.RunWith
import org.scalatestplus.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class DueDateTest extends AnyFunSuite {
  test("countCalendarDaysFromHours works correctly within the current day") {
    val date = Date(2020, 12, 1)
    val hoursToWork = 7
    assertResult((0, 7))(DueDate.countCalendarDaysFromHours(date, hoursToWork))
  }

  test("countCalendarDaysFromHours works correctly within exactly one day") {
    val date = Date(2020, 12, 1)
    val hoursToWork = 8
    assertResult((1, 0))(DueDate.countCalendarDaysFromHours(date, hoursToWork))
  }

  test("countCalendarDaysFromHours works correctly within next day") {
    val date = Date(2020, 12, 1)
    val hoursToWork = 11
    assertResult((1, 3))(DueDate.countCalendarDaysFromHours(date, hoursToWork))
  }

  test("countCalendarDaysFromHours works correctly when exactly falling to Weekend") {
    val date = Date(2020, 12, 2)
    val hoursToWork = 24
    assertResult((5, 0))(DueDate.countCalendarDaysFromHours(date, hoursToWork))
  }

  test("countCalendarDaysFromHours works correctly when falling to middle of the Weekend") {
    val date = Date(2020, 12, 2)
    val hoursToWork = 35
    assertResult((6, 3))(DueDate.countCalendarDaysFromHours(date, hoursToWork))
  }

  test("countCalendarDaysFromHours works correctly when exactly falling to Weekend within several weeks") {
    val date = Date(2020, 12, 2)
    val hoursToWork = 75
    assertResult((13, 3))(DueDate.countCalendarDaysFromHours(date, hoursToWork))
  }

  test("countCalendarDaysFromHours works correctly over 2020") {
    val date = Date(2019, 12, 27)
    val hoursToWork = 2136
    assertResult((375, 0))(DueDate.countCalendarDaysFromHours(date, hoursToWork))
  }

  test("countDueDateFromDays works correctly within same year same month") {
    val date = Date(2020, 12, 14)
    val daysToPass = 8
    assertResult(Date(2020, 12, 22))(DueDate.countDueDateFromDays(date, daysToPass))
  }

  test("countDueDateFromDays works correctly with one day over year") {
    val date = Date(2020, 12, 31)
    val daysToPass = 1
    assertResult(Date(2021, 1, 1))(DueDate.countDueDateFromDays(date, daysToPass))
  }

  test("countDueDateFromDays works correctly with one day over month") {
    val date = Date(2020, 4, 30)
    val daysToPass = 1
    assertResult(Date(2020, 5, 1))(DueDate.countDueDateFromDays(date, daysToPass))
  }

  test("countDueDateFromDays works correctly with one day over leap month") {
    val date = Date(2020, 2, 28)
    val daysToPass = 1
    assertResult(Date(2020, 2, 29))(DueDate.countDueDateFromDays(date, daysToPass))
  }

  test("countDueDateFromDays works correctly with one day over leap month to March") {
    val date = Date(2020, 2, 29)
    val daysToPass = 1
    assertResult(Date(2020, 3, 1))(DueDate.countDueDateFromDays(date, daysToPass))
  }

  test("countDueDateFromDays works correctly with one day") {
    val date = Date(2015, 3, 11)
    val daysToPass = 1
    assertResult(Date(2015, 3, 12))(DueDate.countDueDateFromDays(date, daysToPass))
  }

  test("countDueDateFromDays works correctly within same year and next month") {
    val date = Date(2019, 2, 21)
    val daysToPass = 10
    assertResult(Date(2019, 3, 3))(DueDate.countDueDateFromDays(date, daysToPass))
  }

  test("countDueDateFromDays works correctly within same year around leap day") {
    val date = Date(2020, 2, 21)
    val daysToPass = 10
    assertResult(Date(2020, 3, 2))(DueDate.countDueDateFromDays(date, daysToPass))
  }

  test("countDueDateFromDays works correctly within same year and across several month") {
    val date = Date(2020, 2, 21)
    val daysToPass = 159
    assertResult(Date(2020, 7, 29))(DueDate.countDueDateFromDays(date, daysToPass))
  }

  test("countDueDateFromDays works correctly within same year and across several month, not in leap year") {
    val date = Date(2021, 5, 1)
    val daysToPass = 183
    assertResult(Date(2021, 10, 31))(DueDate.countDueDateFromDays(date, daysToPass))
  }

  test("countDueDateFromDays works correctly in next year") {
    val date = Date(2020, 12, 5)
    val daysToPass = 29
    assertResult(Date(2021, 1, 3))(DueDate.countDueDateFromDays(date, daysToPass))
  }

  test("countDueDateFromDays works correctly in next leap year") {
    val date = Date(2019, 12, 5)
    val daysToPass = 104
    assertResult(Date(2020, 3, 18))(DueDate.countDueDateFromDays(date, daysToPass))
  }

  test("countDueDateFromDays works correctly in near one year") {
    val date = Date(2019, 12, 31)
    val daysToPass = 366
    assertResult(Date(2020, 12, 31))(DueDate.countDueDateFromDays(date, daysToPass))
  }

  test("countDueDateFromDays works correctly in near one year, not leap") {
    val date = Date(2020, 12, 31)
    val daysToPass = 365
    assertResult(Date(2021, 12, 31))(DueDate.countDueDateFromDays(date, daysToPass))
  }

  test("countDueDateFromDays works correctly in one year plus one, not leap") {
    val date = Date(2020, 12, 31)
    val daysToPass = 366
    assertResult(Date(2022, 1, 1))(DueDate.countDueDateFromDays(date, daysToPass))
  }

  test("countDueDateFromDays works correctly in one year plus one, leap") {
    val date = Date(2019, 12, 31)
    val daysToPass = 367
    assertResult(Date(2021, 1, 1))(DueDate.countDueDateFromDays(date, daysToPass))
  }

  test("countDueDateFromDays works correctly in several years") {
    val date = Date(2015, 6, 27)
    val daysToPass = 2145
    assertResult(Date(2021, 5, 11))(DueDate.countDueDateFromDays(date, daysToPass))
  }

  test("countDueDateFromDays works correctly in a few hundred years") {
    val date = Date(2019, 12, 31)
    val daysToPass = 95629
    assertResult(Date(2281, 10, 27))(DueDate.countDueDateFromDays(date, daysToPass))
  }

  test("countDueDateFromDays works correctly over 2020") {
    val date = Date(2019, 12, 27)
    val daysToPass = 374
    assertResult(Date(2021, 1, 4))(DueDate.countDueDateFromDays(date, daysToPass))
  }

  test("dateFromYearStart works correctly within several month") {
    val year = 2020
    val numOfDays = 183
    assertResult(Date(2020, 7, 1))(DueDate.dateFromYearStart(year, numOfDays))
  }

  test("dateFromYearStart works correctly with less than a month") {
    val year = 2020
    val numOfDays = 11
    assertResult(Date(2020, 1, 11))(DueDate.dateFromYearStart(year, numOfDays))
  }

  test("dateFromYearStart works correctly with exactly one month") {
    val year = 2020
    val numOfDays = 30
    assertResult(Date(2020, 1, 30))(DueDate.dateFromYearStart(year, numOfDays))
  }

  test("dateFromYearStart works correctly with near a month") {
    val year = 2020
    val numOfDays = 31
    assertResult(Date(2020, 1, 31))(DueDate.dateFromYearStart(year, numOfDays))
  }

  test("dateFromYearStart works correctly with after one month") {
    val year = 2020
    val numOfDays = 32
    assertResult(Date(2020, 2, 1))(DueDate.dateFromYearStart(year, numOfDays))
  }

  test("dateFromYearStart works correctly with approximately 12 month") {
    val year = 2020
    val numOfDays = 353
    assertResult(Date(2020, 12, 18))(DueDate.dateFromYearStart(year, numOfDays))
  }

  test("calculateDueDate works correctly within one work day") {
    val date = Date(2020, 12, 2)
    val time = Time(10, 37, 59)
    val turnAroundTime = 5
    val (dueDate, dueTime) = DueDate.calculateDueDate(date, time, turnAroundTime)
    assertResult(Date(2020, 12, 2))(dueDate)
    assertResult(Time(15, 37, 59))(dueTime)
  }

  test("calculateDueDate works correctly over one work day") {
    val date = Date(2020, 12, 2)
    val time = Time(10, 37, 59)
    val turnAroundTime = 9
    val (dueDate, dueTime) = DueDate.calculateDueDate(date, time, turnAroundTime)
    assertResult(Date(2020, 12, 3))(dueDate)
    assertResult(Time(11, 37, 59))(dueTime)
  }

  test("calculateDueDate works correctly at end of work day") {
    val date = Date(2020, 12, 2)
    val time = Time(15, 58, 57)
    val turnAroundTime = 2
    val (dueDate, dueTime) = DueDate.calculateDueDate(date, time, turnAroundTime)
    assertResult(Date(2020, 12, 3))(dueDate)
    assertResult(Time(9, 58, 57))(dueTime)
  }

  test("calculateDueDate works correctly at end of Friday") {
    val date = Date(2020, 12, 4)
    val time = Time(15, 58, 57)
    val turnAroundTime = 2
    val (dueDate, dueTime) = DueDate.calculateDueDate(date, time, turnAroundTime)
    assertResult(Date(2020, 12, 7))(dueDate)
    assertResult(Time(9, 58, 57))(dueTime)
  }

  test("calculateDueDate works correctly over weekend") {
    val date = Date(2020, 12, 3)
    val time = Time(15, 58, 57)
    val turnAroundTime = 11
    val (dueDate, dueTime) = DueDate.calculateDueDate(date, time, turnAroundTime)
    assertResult(Date(2020, 12, 7))(dueDate)
    assertResult(Time(10, 58, 57))(dueTime)
  }

  test("calculateDueDate works correctly over month") {
    val date = Date(2020, 11, 30)
    val time = Time(12, 0, 0)
    val turnAroundTime = 26
    val (dueDate, dueTime) = DueDate.calculateDueDate(date, time, turnAroundTime)
    assertResult(Date(2020, 12, 3))(dueDate)
    assertResult(Time(14, 0, 0))(dueTime)
  }

  test("calculateDueDate works correctly over leap month") {
    val date = Date(2020, 2, 27)
    val time = Time(15, 12, 0)
    val turnAroundTime = 26
    val (dueDate, dueTime) = DueDate.calculateDueDate(date, time, turnAroundTime)
    assertResult(Date(2020, 3, 4))(dueDate)
    assertResult(Time(9, 12, 0))(dueTime)
  }

  test("calculateDueDate works correctly over a few month") {
    val date = Date(2020, 1, 28)
    val time = Time(16, 12, 0)
    val turnAroundTime = 193
    val (dueDate, dueTime) = DueDate.calculateDueDate(date, time, turnAroundTime)
    assertResult(Date(2020, 3, 3))(dueDate)
    assertResult(Time(9, 12, 0))(dueTime)
  }

  test("calculateDueDate works correctly over a year") {
    val date = Date(2019, 12, 27)
    val time = Time(11, 34, 0)
    val turnAroundTime = 2136
    val (dueDate, dueTime) = DueDate.calculateDueDate(date, time, turnAroundTime)
    assertResult(Date(2021, 1, 5))(dueDate)
    assertResult(Time(11, 34, 0))(dueTime)
  }
}
