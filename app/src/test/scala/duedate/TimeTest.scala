package duedate

import org.scalatest.funsuite.AnyFunSuite
import org.junit.runner.RunWith
import org.scalatestplus.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class TimeTest extends AnyFunSuite {
  test("Time.apply does not accept invalid hour") {
    assertThrows[IllegalArgumentException] {
      Time(24, 11, 11)
    }
  }

  test("Time.apply does not accept invalid negative hour") {
    assertThrows[IllegalArgumentException] {
      Time(-2, 11, 11)
    }
  }

  test("Time.apply does not accept invalid minute") {
    assertThrows[IllegalArgumentException] {
      Time(0, 60, 11)
    }
  }

  test("Time.apply does not accept invalid negative minute") {
    assertThrows[IllegalArgumentException] {
      Time(0, -1, 11)
    }
  }

  test("Time.apply does not accept invalid sec") {
    assertThrows[IllegalArgumentException] {
      Time(23, 11, 60)
    }
  }

  test("Time.apply does not accept invalid negative sec") {
    assertThrows[IllegalArgumentException] {
      Time(23, 11, -3)
    }
  }
}
