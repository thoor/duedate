package duedate

import duedate.Day._
import org.scalatest.funsuite.AnyFunSuite
import org.junit.runner.RunWith
import org.scalatestplus.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class DateTest extends AnyFunSuite {
  test("Date.apply does not accept invalid year") {
    assertThrows[IllegalArgumentException] {
      Date(-3, 11, 11)
    }
  }

  test("Date.apply does not accept invalid month") {
    assertThrows[IllegalArgumentException] {
      Date(1987, 14, 11)
    }
  }

  test("Date.apply does not accept 0 as a  month") {
    assertThrows[IllegalArgumentException] {
      Date(1987, 0, 11)
    }
  }

  test("Date.apply does not accept invalid day") {
    assertThrows[IllegalArgumentException] {
      Date(1985, 6, 31)
    }
  }

  test("Date.apply does not accept 0 as a  day") {
    assertThrows[IllegalArgumentException] {
      Date(1985, 6, 0)
    }
  }

  test("Date.apply does not accept invalid day in not leap year") {
    assertThrows[IllegalArgumentException] {
      Date(1999, 2, 29)
    }
  }

  test("isLeapYear works correctly on year that is divisible by 4") {
    assertResult(true)(Date.isLeapYear(1996))
  }

  test("isLeapYear works correctly on year that is NOT divisible by 4") {
    assertResult(false)(Date.isLeapYear(1997))
  }

  test("isLeapYear works correctly on year that is divisible by 100") {
    assertResult(false)(Date.isLeapYear(1900))
  }

  test("isLeapYear works correctly on year that is divisible by 400") {
    assertResult(true)(Date.isLeapYear(2000))
  }

  test("numOfDaysPassedInYear works correctly on January") {
    val date = Date(2021, 1, 9)
    assertResult(9)(date.numOfDaysPassedInYear)
  }

  test("numOfDaysPassedInYear works correctly in leap year") {
    val date = Date(2020, 3, 7)
    assertResult(67)(date.numOfDaysPassedInYear)
  }

  test("numOfDaysRestInYear works correctly on December") {
    val date = Date(2020, 12, 26)
    assertResult(5)(date.numOfDaysRestInYear)
  }

  test("numOfDaysRestInYear works correctly on new year") {
    val date = Date(2019, 1, 1)
    assertResult(364)(date.numOfDaysRestInYear)
  }

  test("numOfDaysRestInYear works correctly in leap year") {
    val date = Date(2020, 2, 15)
    assertResult(320)(date.numOfDaysRestInYear)
  }

  test("isBiggerThan works correctly, when bigger") {
    val date = Date(2020, 2, 15)
    assertResult(true)(date.isBiggerThan(Date(2017, 11, 14)))
  }

  test("isBiggerThan works correctly, when smaller") {
    val date = Date(2020, 2, 15)
    assertResult(false)(date.isBiggerThan(Date(2021, 2, 21)))
  }

  test("isBiggerThan works correctly, when year equals") {
    val date = Date(2020, 2, 15)
    assertResult(false)(date.isBiggerThan(Date(2020, 11, 14)))
  }

  test("isBiggerThan works correctly, when year and month equals") {
    val date = Date(2020, 2, 15)
    assertResult(true)(date.isBiggerThan(Date(2020, 2, 14)))
  }

  test("isBiggerThan works correctly, when year, month, day equals") {
    val date = Date(2020, 2, 15)
    assertResult(true)(date.isBiggerThan(Date(2020, 2, 15)))
  }

  test("numOfDaysBetween works correctly in one year distance") {
    val date = Date(2021, 1, 9)
    assertResult(14)(date.numOfDaysBetween(Date(2020, 12, 26)))
  }

  test("numOfDaysBetween works correctly in one year distance mirrored") {
    val date = Date(2020, 12, 26)
    assertResult(14)(date.numOfDaysBetween(Date(2021, 1, 9)))
  }

  test("numOfDaysBetween works correctly in same year") {
    val date = Date(2020, 12, 26)
    assertResult(321)(date.numOfDaysBetween(Date(2020, 2, 9)))
  }

  test("numOfDaysBetween works correctly on same Date") {
    val date = Date(2021, 1, 9)
    assertResult(0)(date.numOfDaysBetween(Date(2021, 1, 9)))
  }

  test("numOfDaysBetween works correctly in 3 year distance") {
    val date = Date(2021, 1, 9)
    assertResult(1096)(date.numOfDaysBetween(Date(2018, 1, 9)))
  }

  test("calculateDayOfWeek works correctly on fix point") {
    val date = Date(2020, 11, 30)
    assertResult(Monday)(date.calculateDayOfWeek)
  }

  test("calculateDayOfWeek works correctly in 2021") {
    val date = Date(2021, 2, 28)
    assertResult(Sunday)(date.calculateDayOfWeek)
  }

  test("calculateDayOfWeek works correctly in 2020") {
    val date = Date(2020, 1, 30)
    assertResult(Thursday)(date.calculateDayOfWeek)
  }

  test("calculateDayOfWeek works correctly in 2020 March") {
    val date = Date(2020, 3, 2)
    assertResult(Monday)(date.calculateDayOfWeek)
  }

  test("calculateDayOfWeek works correctly in 1988") {
    val date = Date(1988, 10, 29)
    assertResult(Saturday)(date.calculateDayOfWeek)
  }

  test("calculateDayOfWeek works correctly in 2018") {
    val date = Date(2018, 5, 11)
    assertResult(Friday)(date.calculateDayOfWeek)
  }
}
